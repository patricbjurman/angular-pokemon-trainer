import { Component, OnInit } from "@angular/core";
import { TrainerService } from "../services/trainer.service";
import { Pokemon } from '../models/pokemon.model';

@Component({
    selector: 'app-trainer-pokemons',
    templateUrl: './trainer-pokemons.component.html',
    styleUrls: ['./trainer-pokemons.component.css']
})

export class TrainerPokemons {
    constructor(private readonly trainerService : TrainerService) {
    }

    get pokemons(): Pokemon[] {
        return this.trainerService.collectedPokemons;
    }

    public handlePokemonClicked(pokemon: Pokemon): void {
        this.trainerService.removePokemon(pokemon);
    }
}