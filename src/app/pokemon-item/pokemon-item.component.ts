import { Component, Input, Output, EventEmitter } from "@angular/core";
import { Pokemon } from '../models/pokemon.model';

// INPUT TO SHOW NAME AND IMG
// OUTPUT TO CLICK EVENT ADD TO COLLECTOR
@Component({
    selector: 'app-pokemon-item',
    templateUrl: './pokemon-item.component.html',
    styleUrls: ['./pokemon-item.component.css']
})

export class PokemonItemComponent {
    @Input() pokemon: Pokemon | undefined;
    @Output() pokeClicked: EventEmitter<Pokemon> = new EventEmitter();
    
    public onPokemonClicked(): void {
        this.pokeClicked.emit(this.pokemon);
    }
}