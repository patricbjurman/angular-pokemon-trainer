import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TrainerPage } from './trainer/trainer.page';
import { LandingPage } from './landing/landing.page';
import { CataloguePage } from './catalogue/catalogue.page';
import { AuthGuard } from './services/auth-guard.service';
import { AuthGuardHaveUser } from './services/auth-guard-have-user.service';

// START PAGE WITH LOGIN
// COLLECT PAGE
// PROFILE PAGE
const routes: Routes = [
  {
    path: '',
    component: LandingPage,
    canActivate:[AuthGuardHaveUser]
  },
  {
    path: 'trainer',
    component: TrainerPage,
    canActivate:[AuthGuard]
  },
  {
    path: 'catalogue',
    component: CataloguePage,
    canActivate:[AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
