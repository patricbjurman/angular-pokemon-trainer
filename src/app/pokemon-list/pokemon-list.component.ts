import { Component, OnInit } from "@angular/core";
import { PokemonsService } from "../services/pokemons.service";
import { Pokemon } from '../models/pokemon.model';
import { TrainerService } from "../services/trainer.service";

@Component({
    selector: 'app-pokemon-list',
    templateUrl: './pokemon-list.component.html',
    styleUrls: ['./pokemon-list.component.css']
})

export class PokemonListComponent implements OnInit {
    constructor(private readonly pokemonService: PokemonsService, private readonly trainerService : TrainerService) {
    }

    ngOnInit(): void {
        this.pokemonService.fetchResource();
        this.pokemonService.setCollected();
    }

    get pokemons(): Pokemon[] {
        return this.pokemonService.pokemons();
    }

    public handlePokemonClicked(pokemon: Pokemon): void {
        this.trainerService.addPokemon(pokemon);
    }
}