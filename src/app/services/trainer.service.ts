import { Injectable } from "@angular/core";
import { Trainer } from "../models/trainer.model";
import { Pokemon } from "../models/pokemon.model";

@Injectable({
    providedIn: 'root'
})
export class TrainerService {
    private _trainer: Trainer = { name: localStorage.key(0), collectedPokemons: [] };

    constructor() { }

    get trainer(): Trainer {
        return this._trainer;
    }

    get collectedPokemons(): Pokemon[] {
        return this._trainer.collectedPokemons;
    }

    public addPokemon(pokemon: Pokemon): void {
        pokemon.collected = true;
        this._trainer.collectedPokemons.push(pokemon);
        let currentPokemons = localStorage.getItem(this._trainer.name as string);
        let pokemonToString = JSON.stringify(pokemon);
        localStorage.setItem(this._trainer.name as string, currentPokemons + pokemonToString + "#");
    }

    public removePokemon(pokemon: Pokemon): void {
        const index = this._trainer.collectedPokemons.indexOf(pokemon);
        this._trainer.collectedPokemons.splice(index, 1);
        let currentPokemons = localStorage.getItem(this._trainer.name as string) as string;
        let pokemonToString = JSON.stringify(pokemon);
        currentPokemons = currentPokemons?.replace(pokemonToString + "#", '')
        localStorage.setItem(this._trainer.name as string, currentPokemons as string);
        pokemon.collected = false;
    }

    public setTrainerName(name: string): void {
        this._trainer.name = name;
    }

    public populateListFromLocalstorage() {
        if (this._trainer.collectedPokemons.length <= 0) {
            if (localStorage.length > 0) {
                let pokemon = localStorage.getItem(this._trainer.name as string) as string;
                let arr = pokemon?.split("#");
                for (let i = 0; i < arr?.length - 1; i++) {
                    let pokeMon = JSON.parse(arr[i]);
                    this._trainer.collectedPokemons.push(pokeMon);
                }
            }
        }
    }
}