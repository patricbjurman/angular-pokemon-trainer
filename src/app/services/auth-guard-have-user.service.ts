import { Injectable }     from '@angular/core';
import { CanActivate }    from '@angular/router';
import { Router } from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class AuthGuardHaveUser implements CanActivate {
    constructor(private router: Router) { }

    canActivate() {
        if (localStorage.length > 0) {
         this.router.navigate(['catalogue']);
        }
        console.log('AuthGuard#canActivate called');
        return true;
    }
}