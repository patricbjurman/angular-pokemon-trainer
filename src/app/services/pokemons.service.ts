import { Injectable } from "@angular/core";
import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { Pokemon } from "../models/pokemon.model";
import { Resource } from "../models/resource.model";
import { TrainerService } from "./trainer.service";

@Injectable({
    providedIn: 'root'
})
export class PokemonsService {

    private _pokemons: Pokemon[] = [];
    private _error: string = '';
    private _resource!: Resource | null;
    private _offset: number = 0;
    private _limit: number = 40;
    private _max: number = 1118;

    constructor(private readonly http: HttpClient, private readonly trainerService: TrainerService) { }

    public fetchResource(): void {
        if (this._pokemons.length <= 0) {
            this.http.get<Resource>(`https://pokeapi.co/api/v2/pokemon/?limit=${this._max}&offset=${this._offset}`)
            .subscribe((Response) => {
                this._resource = Response;
                this._pokemons = this._resource.results.map((pokemon: Pokemon) => {
                    const urlSplit = pokemon.url.split('/');
                    return { ...pokemon, id: urlSplit[urlSplit.length - 2] }
                });
            }, (error: HttpErrorResponse) => {
                this._error = error.message;
            });
        }
    }

    public pokemons(): Pokemon[] {
        return this._pokemons.slice(this._offset, this._offset + this._limit);
    }

    public allPokemons(): Pokemon[] {
        return this._pokemons;
    }

    public setCollected(): void {
        if (localStorage.length > 0) {
            this.trainerService.populateListFromLocalstorage();
            if (this._pokemons.length > 0) {
                let arr = this._pokemons;
                let result = arr.filter(p1 => this.trainerService.collectedPokemons.some(p2 => p1.name === p2.name));
                console.log(result);
            }
        }
    }

    public setOffset(offset: number): void {
        if (this._offset + offset >= 0) {
            this._offset += offset;
        }
    }

    public error(): string {
        return this._error;
    }
}