import { Component } from "@angular/core";
import { Trainer } from '../models/trainer.model';
import { TrainerService } from "../services/trainer.service";
import { Router } from "@angular/router";

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.css']
})

export class NavbarComponent {

    constructor(private router: Router, private readonly trainerService: TrainerService) {}

    getTrainerName():string|null {
        return this.trainerService.trainer.name;
    }

    goToTrainerPage():void {
        this.router.navigate(['trainer']);
    }

    goToCataloguePage():void {
        this.router.navigate(['catalogue']);
    }

    logOut():void {
        localStorage.clear();
        this.router.navigate(['']);
    }

    checkLocalStorage(): boolean {
        return localStorage.length > 0 ? true : false;
    }
}