import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PokemonListComponent } from './pokemon-list/pokemon-list.component';
import { PokemonItemComponent } from './pokemon-item/pokemon-item.component';
import { LandingPage } from './landing/landing.page';
import { NavbarComponent } from './navbar/navbar.component';
import { TrainerPage } from './trainer/trainer.page';
import { CataloguePage } from './catalogue/catalogue.page';
import { TrainerPokemons } from './trainer-pokemons/trainer-pokemons.component';

@NgModule({
  declarations: [
    AppComponent,
    PokemonListComponent,
    PokemonItemComponent,
    LandingPage,
    TrainerPage,
    CataloguePage,
    NavbarComponent,
    TrainerPokemons
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
