import { Component } from "@angular/core";
import { TrainerService } from "../services/trainer.service";

@Component({
    selector: 'app-profile-page',
    templateUrl: './trainer.page.html',
    styleUrls: ['./trainer.page.css']
})
export class TrainerPage {
    constructor(private readonly trainerService: TrainerService) { }

    ngOnInit() {
        this.trainerService.populateListFromLocalstorage();
    }
}