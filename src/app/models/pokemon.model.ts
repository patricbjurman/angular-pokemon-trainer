export interface Pokemon {
    id?: string;
    name: string;
    url: string;
    collected: boolean;
}