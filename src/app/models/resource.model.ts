export interface Resource {
    count: number;
    next: string;
    previous: boolean;
    results: [];
}