import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { TrainerService } from "../services/trainer.service";
import { PokemonsService } from "../services/pokemons.service";

@Component({
    selector: 'app-landing-page',
    templateUrl: './landing.page.html',
    styleUrls: ['./landing.page.css']
})
export class LandingPage {

    constructor(private router: Router, private readonly trainerService: TrainerService, private readonly pokemonService: PokemonsService) {   
    }
    
    public onLoginButtonClicked(name : string) {
        if (name != '') {
            if (localStorage.getItem(name) == null) {
                localStorage.setItem(name, '');
            }
            this.trainerService.setTrainerName(name);
            this.router.navigate(['catalogue']);
        } else {
            alert("Can't be empty");
        }

    }
}