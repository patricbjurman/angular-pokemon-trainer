import { Component } from "@angular/core";
import { PokemonsService } from "../services/pokemons.service";

@Component({
    selector: 'app-catalogue-page',
    templateUrl: './catalogue.page.html',
    styleUrls: ['./catalogue.page.css']
})
export class CataloguePage {
    constructor(private readonly pokemonService: PokemonsService) { }

    public previousPage(): void {
        this.pokemonService.setOffset(-40);
    }

    public nextPage(): void {
        this.pokemonService.setOffset(40);
    }
}